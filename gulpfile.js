var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {

mix
//.less('app.less', 'public/css/app.css')

    .sass(['app.scss'], 'public/css/all.css')

    .scripts([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'bower_components/typed.js/dist/typed.min.js',
        'bower_components/vue/dist/vue.min.js',
        'bower_components/vue-resource/dist/vue-resource.min.js',
        'bower_components/jquery_lazyload/jquery.lazyload.js',
        'bower_components/smooch/dist/smooch.js',
        'bower_components/toastr/toastr.min.js',
        'js/app.js',
        'js/vm.js'
    ], null, 'resources/assets')

    .copy('resources/assets/fonts', 'public/build/fonts')
    .copy('resources/assets/bower_components/Dripicons/fonts', 'public/build/fonts/dripicons')
    .copy('resources/assets/image', 'public/build/image')

    .version([
        'css/all.css',
        'js/all.js'
    ]);

elixir.Task.find('sass').watch('resources/assets/sass/app.scss');
});
