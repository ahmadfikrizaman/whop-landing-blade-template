Vue.http.headers.common['X-CSRF-TOKEN'] = $("meta[name='X-CSRF-TOKEN']").attr('content');

Smooch.init({
    appToken: 'ev7i6ip0c9ruceyt6j1268bf9'
});

var socket = io();
// var socket = io.connect('http://localhost:4040');

var sockPush = {
    e: $("meta[name='x-sock-email']").attr('content'),
    k: $("meta[name='x-sock-sub']").attr('content'),
};

var sockdata = sockPush.e + ':' + sockPush.k;

/**
 * Url Active
 * @type {[type]}
 */
var url = window.location;
$('ul.nav a[href="' + url + '"]').parent().addClass('active');

$('ul.nav a').filter(function() {
    return this.href == url;
}).parent().addClass('active');

$(window).on('load resize', function() {
    var width = $(window).width(),
        height = $(window).height();
    if (width <= 768) {
        $('.navbar-default').addClass('navbar-fixed-top');
    } else {
        $('.navbar-default').removeClass('navbar-fixed-top');
    }
});

/**
 * Spy sidenav
 * @type {String}
 */
$("body").scrollspy({
    target: "#side-nav",
    offset: 50
});

/**
 * Carousel
 */
$('.carousel').carousel({
    interval: 5000
});

/**
 * Lazyload
 */
$("img.lazy").lazyload();


/**
 * Component
 */
Vue.component('vue-messages', {
    template: '#vuemessages',

    props: {
        isbusy: true,
    },

    data: function() {
        return {
            errorMessages: [],
            successMessage: null,
        };
    },

    events: {
        'error-message': function(error) {
            this.clearError();
            this.generateError(error);
        },

        'success-message': function(success) {
            this.clearError();
            this.generateSuccess(success);
        },

        'clear-error': function() {
            this.clearError();
        }
    },

    methods: {
        generateError: function(error) {
            this.isbusy = false;

            if (error.status == 403) {
                this.errorMessages.push(error.data);
            } else if (error.status == 500) {
                this.errorMessages.push('Internal server error. Please refresh your browser.');
            }
        },

        generateSuccess: function(success) {

            this.isbusy = false;

            var data = success.data;

            if (data.success == true) {
                this.successMessage = data.message;
            }
        },

        clearError: function() {
            this.errorMessages = [];
            this.successMessage = null;
        }
    },
});

$(window).on('load resize', function() {
    $("#video").width($(window).width()).height($(window).height());
});


// youtube player
var player;

function onYouTubePlayerAPIReady() {
    player = new YT.Player('video', {
        events: {
            'onReady': onPlayerReady
        }
    });
}

function onPlayerReady(event) {

    var playButton = document.getElementById("play-button");
    playButton.addEventListener("click", function() {
        player.playVideo();
    });

    var pauseButton = document.getElementById("stop-button");
    pauseButton.addEventListener("click", function() {
        player.pauseVideo();
    });

}

var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
