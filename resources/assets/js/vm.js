'use strict'

new Vue({
    el: '#indexVM',

    data: {
        isBusy: false,
        typedData: [
            "PHP Applications",
            "Wordpress",
            "E-Commerce",
            "CMS",
            "Laravel",
            "Symphony",
            "Code Igniter",
            "CakePHP",
        ],
        registerdata: {
            name: null,
            email: null,
            password: null,
            confirmPassword: null,
        },
    },

    ready: function() {

        var _self = this;

        $("#technology").typed({
            strings: _self.typedData,
            typeSpeed: 0,
            loop: true,
            backDelay: 2000,
        });
    },

    methods: {
        register: function(e) {

            this.clearError();

            var url = e.target.action;

            this.isBusy = true;

            this.$http({url: url, method: 'POST', data: this.registerdata}).then( function success(success) {
                this.$broadcast('success-message', success);

                this.registerdata = {};
            }, function error(error) {
                
                this.$broadcast('error-message', error);

                if (error.status == 422) {
                    var errors = error.data;

                    $.each(errors, function(index, value) {
                        $('.help-block[for="' + index + '"]').removeClass('hide');
                        $('.help-block[for="' + index + '"]').text(value[0]);
                        $('[name=' + index + ']').parent().addClass('has-error');
                    });
                }

            });
        },

        clearError: function() {
            $('.help-block').addClass('hide');
            $('.has-error').removeClass('has-error');
        },
    }
});


new Vue({

    el: '#licenseVM',

    ready: function() {

        console.log(sockdata + '#GenerateLicense');

        socket.on(sockdata + '#GenerateLicense', function(data) {
            toastr.success(data.message, 'License Generated!!!');
            
            $('#' + data.key).html('<a type="button" class="btn btn-success btn-xs" href="' + data.url + '">Download</a>');
        });
    },

});