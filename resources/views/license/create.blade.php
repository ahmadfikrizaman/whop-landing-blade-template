@extends('layouts.master', ['title' => 'Create License'])

@section('content')

    <section class="create-license">
        <div class="row header">
            <div class="col-md-12">
                <h1>Create License</h1>
                <h6>WHoP is a subscription based service. We got you covered.</h6>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                    <div class="panel">
                        <div class="panel-body">


                            @include('partials.messages')


                            <form action="{{ route('license:store') }}" method="POST" role="form">

                                {{ csrf_field() }}

                                <div class="form-group {{ $errors->has('domainName') ? 'has-error' : null }}">
                                    <label for="">Domain Name</label>
                                    <input type="text" class="form-control input-lg" name="domainName" placeholder="Your top level domain name">
                                    <span class="help-block {{ $errors->has('domainName') ? 'null' : 'hide' }}">{{ $errors->first('domainName') }}</span>
                                </div>

                                <div class="form-group {{ $errors->has('ipAddress') ? 'has-error' : null }}">
                                    <label for="">IP Address</label>
                                    <input type="text" class="form-control input-lg" name="ipAddress" placeholder="Your IP Address for main server">
                                    <span class="help-block {{ $errors->has('ipAddress') ? 'null' : 'hide' }}">{{ $errors->first('ipAddress') }}</span>
                                </div>

                                <div class="form-group {{ $errors->has('licenseType') ? 'has-error' : null }}">
                                    <label for="">License Type</label>
                                    <select name="licenseType" class="form-control input-lg" required="required">
                                        @foreach($licenses as $license)
                                            <option value="{{ $license->id }}">{{ $license->packageName }} (${{ $license->price }})</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block {{ $errors->has('licenseType') ? 'null' : 'hide' }}">{{ $errors->first('licenseType') }}</span>
                                </div>

                                <div class="form-group {{ $errors->has('coupon') ? 'has-error' : null }}">
                                    <label for="">Coupon</label>
                                    <input type="text" class="form-control input-lg" name="coupon" placeholder="Enter coupon code if you have any">
                                    <span class="help-block {{ $errors->has('coupon') ? 'null' : 'hide' }}">{{ $errors->first('coupon') }}</span>
                                </div>

                                <button type="submit" class="btn btn-success btn-lg pull-right">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
