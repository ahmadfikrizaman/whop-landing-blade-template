@extends('layouts.master', ['title' => 'My License'])

@section('content')

    <section class="list-license" id="licenseVM">
        <div class="row header">
            <div class="col-md-12">
                <h1>My License</h1>
                <h6>Your subscription.</h6>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                @include('partials.messages')

                @if($licenses->count())
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Domain</th>
                                        <th>IP Address</th>
                                        <th>Created</th>
                                        <th>Expiration Date</th>
                                        <th>Download</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($licenses as $license)
                                        <tr>
                                            <td>{{ $license->domain }}</td>
                                            <td>{{ $license->ipAddress }}</td>
                                            <td>{{ date('d F Y', strtotime($license->created_at)) }} ({{ $license->created_at->diffForHumans() }})</td>
                                            <td>
                                                @if ($license->endDate != null)
                                                    {{ date('d F Y', strtotime($license->endDate)) }} ({{ $license->endDate->diffForHumans() }})
                                                @else
                                                    No Data
                                                @endif
                                            </td>
                                            <td id="{{ $license->serialNumber }}">
                                                @if ($license->download == false && $license->invoice->price == 0)
                                                    Generating
                                                @elseif ($license->download == false && $license->invoice->price != 0)
                                                    Button Pay
                                                @elseif ($license->download == true )
                                                    <a type="button" class="btn btn-success btn-xs" href="{{ route('license:download', $license->serialNumber) }}">Download</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @else
                    <div class="panel panel-item-not-exist">
                        <div class="panel-body text-center">
                            <img src="/build/image/license.png">
                            <h3>Looks like you don’t have any subscription</h3>
                            <h5>In the nick of time, you can have your license too</h5>
                            <a href="{{ route('license:create') }}" class="btn btn-success btn-lg">Subscribe now</a>
                        </div>
                    </div>
                @endif
                {!! $licenses->render() !!}
                </div>
            </div>
        </div>
    </section>

@stop
