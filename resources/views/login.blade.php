@extends('layouts.plain', ['title' => 'Login'])

@section('content')
    <section class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="panel">
                        <div class="panel-body">
                            <a href="{{ route('index') }}"><img alt="Brand" src="/build/image/logo.png"></a>

                            @include('partials.messages')

                            <form action="{{ route('login:submit') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : null }}">
                                    <input type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Email Address">
                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : null }}">
                                    <input type="password" class="form-control input-lg" name="password" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group text-left">
                                    <label>
                                        <input type="checkbox" name="alwaysLogin"> Keep me logged in
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-special btn-lg btn-block">Login</button>
                                </div>
                                <a href="{{ route('forgot') }}" class="forgot pull-right">Forgot Password?</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <span>Don't have an account?</span>
                        <a type="button" class="btn btn-lg btn-default" href="{{ route('index') }}">Sign Up</a>
                    </div>
                </div>
                <h6><a href="{{ route('verify:create') }}"> Resend Email Confirmation </a></h6>
            </div>
        </div>
    </section>
@stop
