@extends('layouts.plain', ['title' => 'Resend Verification Email'])

@section('content')
    <section class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="header">Recover Your Password</h3>
                            <p class="sub-header">Enter your email and we’ll send you a password reset link.</p>
                            <span class="help-block has-error">Internal Server Error</span>
                            <form action="{{ route('verify:store') }}" method="POST">

                                {{ csrf_field() }}

                                <div class="form-group has-error">
                                    <input type="email" class="form-control input-lg" name="email" placeholder="Email Address" value="{{ old('email') }}">
                                    <span class="help-block">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-special btn-lg btn-block">Send</button>
                                </div>
                                <a href="{{ route('login') }}" class="forgot pull-left"><i class="dripicons-chevron-left"></i> Back to Login</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
