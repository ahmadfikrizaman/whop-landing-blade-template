@extends('layouts.mail')

@section('content')

<p>Hello {{ $name }},</p><br>

<p style="line-height:1.7">Thank you for registering with whop.io. We hope that you are eager to use our application to make your
server administration easier than ever. Before you can create your own whop.io license, you have to verify
this email to make sure that your email is exist. Please click the link below to activate your account.</p>
<p style="text-align:center">
   <a href="{{ $url }}" style="background-color:#5f93e7;border:0;border-radius:3px;color:#ffffff!important;display:inline-block;font-family:'Open Sans',Calibri,Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:700;outline:0;padding:12px 24px;text-align:center;text-decoration:none!important">Verify Email</a>
</p>
@endsection
