@extends('layouts.plain', ['title' => 'Register'])

@section('content')
    <section class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="header">It only take 7 seconds</h3>
                            <p class="sub-header">We’ll send you a email confirmation.</p>
                            <span class="help-block has-error">Internal Server Error</span>
                            <form action="{{ route('register:submit') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group has-error">
                                    <input type="text" class="form-control input-lg" v-model="registerdata.name" placeholder="Name or company name">
                                    <span class="help-block">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-lg" v-model="registerdata.email" placeholder="Email Address">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control input-lg" v-model="registerdata.password" placeholder="Your password">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control input-lg" v-model="registerdata.confirmPassword" placeholder="Confirm your password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-special btn-lg btn-block">Sign Up</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <span>Already a member?</span>
                        <a type="button" class="btn btn-lg btn-default" href="{{ route('login') }}">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
