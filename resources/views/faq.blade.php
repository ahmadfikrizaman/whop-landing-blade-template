@extends('layouts.master', ['title' => 'FAQ'])

@section('content')

    <section class="faq-page">
        <div class="row header">
            <div class="col-md-12">
                <h1>Frequently Asked Questions</h1>
                <h6>Find the answer of the frequently asked questions</h6>
            </div>
        </div>
        <div class="container">
            <div class="row text-left">
                <div class="col-md-3" id="side-nav">
                    <ul class="nav nav-tabs tabs-left side-menu affix" role="tablist" data-spy="affix" data-offset-top="60">
                        @foreach($faqs as $index => $faq)
                            <li><a href="#c{{ array_search($index,array_keys($faqs)) }}">{{ $index }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-md-9 faq-main">
                    @foreach($faqs as $index => $faq)
                        <div id="c{{ array_search($index,array_keys($faqs)) }}">
                            <h3># {{ $index }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($faq as $obj)
                                        <h5>{{ $obj['question'] }}</h5>
                                        <h6>{!! $obj['answer'] !!}</h6>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@stop
