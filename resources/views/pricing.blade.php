@extends('layouts.master', ['title' => 'Pricing'])

@section('content')

    <section class="pricing-page">
        <div class="row header">
            <div class="col-md-12">
                <h1>Straightforward Pricing</h1>
                <h6>WHoP is a subscription based service. We have plan for everyone.</h6>
            </div>
        </div>
        <div class="container center-block">
            <table class="table table-bordered">
                <thead>
                    <th class="col-lg-4 col-md-4">
                        <h4 class="package-select hidden-sm hidden-xs">Select the package that best suits of your needs.</h4>
                        <h6 class="package-select-sub hidden-sm hidden-xs">Pay monthly or save big with an annual subscription.</h6>
                    </th>
                    @foreach ($packages as $package)
                        <th class="{{ $package->recommended ? 'active' : null }} col-lg-2 col-md-2">
                            <h5 class="package-name">{{ $package->packageName }}</h5>
                            <h1 class="package-price"><sup>$</sup>{{ $package->price }}</h1>
                            @if ($package->recommended)
                                <h6>Most Recommended</h6>
                            @endif
                        </th>
                    @endforeach
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/periodic_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Period Validity</div>
                                    <div class="description">The length of time your license last</div>
                                </div>
                            </div>
                        </td>
                        @foreach ($packages as $package)
                            <td>
                                <h5>{{ $package->validity }}</h5>
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/eligibility_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Eligibility</div>
                                    <div class="description">License usage</div>
                                </div>
                            </div>
                        </td>
                        @foreach ($packages as $package)
                        <td>
                            <h5>{{ $package->eligibility }}</h5>
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/whop_line_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">WHoPlets</div>
                                    <div class="description">Number of website you can build</div>
                                </div>
                            </div>
                        </td>
                        @foreach ($packages as $package)
                        <td>
                            @if ($package->numberOfWhoplet == 0)
                                <h5>Unlimited</h5>
                            @else
                                <h5>{{ $package->numberOfWhoplet }}</h5>
                            @endif
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/clients_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Multiple Clients</div>
                                    <div class="description">Whether you can have more than 1 user in your server</div>
                                </div>
                            </div>
                        </td>
                        @foreach ($packages as $package)
                            <td>
                                @if ($package->multipleClient == true)
                                    <i class="dripicons-checkmark"></i>
                                @endif
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/update_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Automatic Updates</div>
                                    <div class="description">Auto update for system and WHoP packages</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/redis_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Dedicated Redis</div>
                                    <div class="description">Redis Instance for each user</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/server_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Multi Servers</div>
                                    <div class="description">Isolate Website, Database and DNS to different servers</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/php_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Multiple PHP Versions</div>
                                    <div class="description">Choose your own PHP version for each WHoPlet</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/domain_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">DNS Management</div>
                                    <div class="description">Point domain name to your server and WHoP will manage it</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/dnssec_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">DNSSEC</div>
                                    <div class="description">DNSSEC enabled domain for domain security</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/email_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Email Management</div>
                                    <div class="description">Manage multiple email for multiple domain name for each client</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/log_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">Live WHoPlet Log</div>
                                    <div class="description">Live checking access and error log for each WHoPlet</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row features">
                                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                    <img src="/build/image/statistic_icon.png">
                                </div>
                                <div class="col-lg-10 col-md-10">
                                    <div class="name">WHoPlet Statistic</div>
                                    <div class="description">Access and Error statistic for each day for each WHoPlet</div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                        <td>
                            <i class="dripicons-checkmark"></i>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        @if (!Auth::check())
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="{{ route('index') }}" class="btn btn-success btn-lg">get started</a>
                </div>
            </div>
        @endif
    </section>

@stop
