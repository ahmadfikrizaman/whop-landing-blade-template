@extends('layouts.master', ['title' => 'Download'])

@section('content')

    <section class="download-page">
        <div class="row">
            <div class="col-md-12 header">
                <h1>Download WHoP</h1>
                <h6>Current version is {{ env('WHOP_VERSION') }} ({{ env('WHOP_CODE_VERSION') }})</h6>
            </div>
        </div>
        <div class="container">
            <div class="row text-left">
                <div class="col-md-3" id="side-nav">
                    <ul class="nav nav-tabs tabs-left side-menu affix" role="tablist" data-spy="affix" data-offset-top="60">
                        <li><a href="#requirement">Requirement</a></li>
                        <li><a href="#installation">Installation</a></li>
                    </ul>
                </div>

                <div class="col-md-9">
                    <div id="requirement">
                        <h3># Requirement</h3>
                        <blockquote>
                            <p>
                                WHoP is a multi server control panel. You can install WHoP on single server but you can have up to four servers. Each server will have their own service (Web Server, DNS Server, Database Server, Email Server). WHoP does not provide uninstaller. If you want to completely uninstall WHoP, you must reformat your server.
                            </p>
                        </blockquote>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Make sure your server meets the following requirements:</h5>
                                <ul class="list-unstyled">
                                    <li><i class="dripicons-chevron-right"></i> WHoP should be installed on freshly-installed operating system.</li>
                                    <li><i class="dripicons-chevron-right"></i> If you are using VPS, you have to make sure it is running on <strong>KVM</strong> or <strong>XEN</strong> or user disk quota will not work.</li>
                                    <li><i class="dripicons-chevron-right"></i> Minimum disk space 20GB for single server. For multiple server, please make sure you have more than 5GB of disk space in your main server.</li>
                                    <li><i class="dripicons-chevron-right"></i> Single core processor is ok but recommended more than 2 core.</li>
                                    <li><i class="dripicons-chevron-right"></i> Create glue record ns1.yourdomain and point it to your main server. E.g: ns1.whop.io ---> 123.123.123.123</li>
                                    <li><i class="dripicons-chevron-right"></i> Create glue record ns2.yourdomain and point it to your main server. E.g: ns2.whop.io ---> 123.123.123.123</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="installation">
                        <h3># Installation</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>SSH into your main server and install git</h5>
                                <pre>yum install git -y</pre>
                                <h5>Go to any directory to install WHoP. This example will use home directory.</h5>
<pre>cd ~
git clone https://bitbucket.org/coolcodemy/whop-installer.git
cd whop-installer
git checkout {{ env('WHOP_CODE_VERSION') }}
</pre>
                                <h5>Create a new text file name servers.txt in the cloned folder and fill your server/s ip and name. Each IP are separated by new line and server's name separated by colon. You should do the same even though you only have single server. E.g:</h5>
<pre>111.111.111.111:Main Server
222.222.222.222:Database Server
333.333.333.333:Email Server
444.444.444.444:DNS Server
</pre>
                                <h5>or</h5>
<pre>111.111.111.111:Main Server
222.222.222.222:Other Server
</pre>
                                <h5>Install the WHoP by running <code> ./installer</code></h5>
                                <pre>./installer</pre>
                                <h5>If you have multiple server, WHoP will make secure connection to each server automatically for communication. Upon installation, WHoP Installer will ask a few question to start your installation. After the installation finished, you will be given the installation info and WHoP setup link.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
