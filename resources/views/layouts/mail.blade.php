<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('PRODUCT_NAME') }}</title>

    <!--[if !mso]><!-- -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <!--<![endif]-->

    <style type="text/css">
        @media only screen and (max-width: 480px) {
            .article-table td {
                display: block !important;
                width: 100% !important;
                text-align: center !important;
            }

            .article-table strong {
                margin: 24px 0 32px !important;
                font-size: 21px !important;
            }

            .table-body {
                padding: 8px !important;
            }

            .content-body {
                padding: 16px !important;
            }

            .top-icon img {
                max-width: 96px !important;
            }
        }
    </style>
</head>
<body style="-webkit-font-smoothing: antialiased; background: #f2f6fa; color: #3c374b; font-family: 'Open Sans', Calibri, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 16px; height: 100%; margin: 0; padding: 0; width: 100%;">
    <table style="border-collapse: collapse; width: 100%;">
        <tr>
            <td class="logo-buddy" style="padding: 48px 0 20px; text-align: center;">
                <img src="https://whop.io/build/image/logo.png" height="50%">
            </td>
        </tr>
    </table>
    <table class="table-wrap" style="border-collapse: collapse; width: 100%;">
        <tr>
            <td></td>
            <td class="table-body" style="clear: both !important; display: block !important; margin: 0 auto !important; max-width: 700px !important; padding: 16px;">
                <div class="content-body" style="background: #ffffff; border-radius: 3px; display: block; margin: 0 auto; max-width: 700px; padding: 32px;">
                    <table class="content-table" style="border-collapse: collapse; width: 100%;">


                        <tr class="top-icon">
                            <td style="display: block; margin: 8px 0 8px; padding-top: 0 !important; text-align: center;">
                                {{-- <img src="{{ url('build/img', $type . '.png') }}"> --}}
                            </td>
                        </tr>

                        <tr>
                            <td class="content" style="padding: 32px 0 32px;">
                                <h2 style="display: block; font-size: 21px; font-weight: bold; line-height: 1.524; margin: 0; margin-bottom: 36px; padding: 0;"> {{ $subject }} </h2>
                                <p style="line-height: 32px; margin-bottom: 0;">
                                    @yield('content')
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="border-collapse: collapse; width: 100%;">
                    <br>
                    <tr>
                        <td class="footer" style="color: #96a2ba; font-size: 12px; padding: 0 0 32px; text-align: center;">
                            <p style="margin: 0;">You recieved this email because you are registered with us.</p>
                        </td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>
