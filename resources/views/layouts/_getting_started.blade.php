<section class="getting-started-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Are you ready to be amazed?</h3>
                <h6>It doesn't hurt.</h6>
                <a href="{{ route('index') }}" class="btn btn-success btn-lg">Get started</a>
            </div>
        </div>
    </div>
</section>
