<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="X-CSRF-TOKEN" content="{{ csrf_token() }}">
    <title>WHoP.io | {{ $title }}</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="apple-touch-icon" sizes="57x57" href="/build/image/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/build/image/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/build/image/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/build/image/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/build/image/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/build/image/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/build/image/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/build/image/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/build/image/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/build/image/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/build/image/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/build/image/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/build/image/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/build/image/manifest.json">
    <link rel="mask-icon" href="/build/image/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    @if (Auth::check())
        <meta name="x-sock-email" content="{{ Auth::user()->email }}">
        <meta name="x-sock-sub" content="{{ Auth::user()->nodeKey }}">
    @endif
    <meta name="theme-color" content="#ffffff">
</head>

<body class="{{ ($title == 'Home') ? 'bg-white' : 'bg-gray' }}">

    <div>
        @include('layouts._nav')
    </div>
    @yield('content')

    @if($title == 'Home' && !Auth::check())
        @include('layouts._getting_started')
    @endif

    @include('layouts._footer')

    <template id="vuemessages">
        <div class="alert alert-danger" v-if="errorMessages.length > 0">
            <p v-for="error in errorMessages">* @{{ error }}</p>
        </div>

        <div class="alert alert-success" v-if="successMessage != null">
            <p>@{{ successMessage }}</p>
        </div>
    </template>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.8/socket.io.min.js"></script>
    <script src="{{ elixir('js/all.js') }}"></script>
</body>

</html>
