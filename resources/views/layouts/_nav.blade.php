<nav class="navbar navbar-default {{ ($title == 'Home') ? 'bg-white' : 'bg-gray' }}">
    <div class="container-fluid navbar-custom">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('index') }}">
                <img alt="Brand" src="/build/image/logo.png">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('index') }}">Home</a></li>
                <li><a href="{{ route('pricing') }}">Pricing</a></li>
                <li><a href="{{ route('download') }}">Download</a></li>
                <li><a href="{{ route('faq') }}">FAQ</a></li>
                <li><a href="{{ route('about') }}">About</a></li>
                @if (Auth::check())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="special"><a>{{ Auth::user()->email }}</a></li>
                        <li role="separator" class="divider"></li>
                        @can('create-license')
                            <li><a href="{{ route('license:create') }}">Create WHoP License</a></li>
                        @endcan
                        @can('list-license')
                            <li><a href="{{ route('license:index') }}">View My License</a></li>
                        @endcan
                        @can('view-order-history')
                            <li><a href="{{ route('order:index') }}">View Order History</a></li>
                        @endcan
                        <li class="special"><a href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </li>
                @endif
                @if (!Auth::check())
                    <li class="special"><a href="{{ route('login') }}">Sign-in</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
