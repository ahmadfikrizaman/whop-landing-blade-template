<section class="footer-page">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h5>Product</h5>
                <ul class="list-unstyled">
                    <li><a href="{{ route('pricing') }}">Pricing</a></li>
                    <li><a href="{{ route('download') }}">Download</a></li>
                    <li><a href="{{ route('index') }}">Sign Up</a></li>
                    <li><a href="{{ route('login') }}">Login</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Contact</h5>
                <ul class="list-unstyled">
                    <li><a>enquiry@whop.io</a></li>
                    <li><a>+60193774212</a></li>
                    <li><a href="https://blog.whop.io/">Blog</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Help</h5>
                <ul class="list-unstyled">
                    <li><a href="{{ route('faq') }}">FAQ</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <img src="/build/image/logo_white.png" class="pull-left">
                <div class="clearfix"></div>
                <h6>Copyright © {{ date('Y') }} WHoP ™ Inc.</h6>
                <h6>Code with love by Cool Code Sdn. Bhd.</h6>
                <a href="{{ route('about') }}" class="btn btn-info pull-left">Be our partner</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5 class="special">5,8374 whoplets installed</h5>
            </div>
        </div>
    </div>
</section>
