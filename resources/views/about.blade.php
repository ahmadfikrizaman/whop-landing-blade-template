@extends('layouts.master', ['title' => 'About'])

@section('content')

    <section class="about-page">
        <div class="row header">
            <div class="col-md-12">
                <h1>About Us</h1>
                <h6>From cool coders to developers</h6>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <img src="/build/image/team.png" style="max-width: 100%">
                </div>
            </div>
        </div>
    </section>
    <section class="contact-page">
        <div class="row header">
            <div class="col-md-12">
                <h1>Contact Us</h1>
                <h6>Feedback? Questions? Blackmails? Crazy new ideas? Drop us a line and we'll get back to you in a snap:</h6>
                <h5> enquiry@whop.io</h5>
                <address>
                    <strong>Cool Code Sdn Bhd</strong><br>
                    T03, Level 1, University Industry Research Lab (UIRL) Building<br>
                    Universiti Teknologi Malaysia<br>
                    81310 Skudai, Johor Bahru, Johor<br>
                    <br>
                    <abbr title="Phone">P:</abbr> +60193774212 | <abbr title="Website"> W:</abbr> <a href="http://coolcode.my/" target="_blank">coolcode.my</a>
                </address>
            </div>
        </div>
    </section>

@stop
