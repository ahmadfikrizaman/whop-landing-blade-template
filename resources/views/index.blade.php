@extends('layouts.master', ['title' => 'Home'])

@section('content')

    <div id="indexVM">
        <section class="landing-page">
            <div class="row header">
                <div class="col-md-12">
                    <h1>WHoP - Hosting Panel for <span id="technology"></span></h1>
                    <h6 class="special">Simple hosting panel. Built for PHP enthusiast.</h6>
                    <p><button class="btn btn-success btn-lg" id="play-button" data-toggle="modal" data-target="#myModal">See it in action </button></p>

                    <div class="modal fade modal-fullscreen force-fullscreen" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" id="stop-button" class="close" data-dismiss="modal" aria-hidden="true">
                                        <img src="/build/image/cancel.png">
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <iframe id="video" src="https://www.youtube.com/embed/GCKPemxfS6k?enablejsapi=1&amp;rel=0&amp;fs=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="row">
                    <div class="{{ (!Auth::check()) ? 'col-lg-8 col-md-12 col-sm-12 col-xs-12' : 'col-lg-10 col-lg-offset-1' }}">
                        <img class="img-responsive" src="/build/image/laptop.png">
                        <img class="whop-walkthrough" src="/build/image/whop_walkthrough.gif">
                    </div>
                    @if (!Auth::check())
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <h3>Create WHoP account for free</h3>
                            <vue-messages :isbusy.sync="isBusy" class="help-block has-error"></vue-messages>

                            <form action="{{ route('register:submit') }}" method="POST" @submit.prevent="register">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <input type="text" name="name" class="form-control input-lg" v-model="registerdata.name" placeholder="Name or company name">
                                    <span class="help-block hide" for="name">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control input-lg" v-model="registerdata.email" placeholder="Email Address">
                                    <span class="help-block hide" for="email">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control input-lg" v-model="registerdata.password" placeholder="Your password">
                                    <span class="help-block hide" for="password">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="confirmPassword" class="form-control input-lg" v-model="registerdata.confirmPassword" placeholder="Confirm your password">
                                    <span class="help-block hide" for="confirmPassword">This field is required</span>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block btn-lg btn-create" :disabled="isBusy">
                                        <span v-show="!isBusy">Create free account</span>
                                        <div class="spinner" v-show="isBusy">
                                          <div class="rect1"></div>
                                          <div class="rect2"></div>
                                          <div class="rect3"></div>
                                          <div class="rect4"></div>
                                          <div class="rect5"></div>
                                        </div>
                                    </button>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </section>
        <section class="highlight-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <h2>Server configuration now easy with simple panel and no pain</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="/build/image/rocket.png">
                        <h3>Fast</h3>
                        <h5>Fast panel with full of optimization for your application.</h5>
                    </div>
                    <div class="col-md-4">
                        <img src="/build/image/success.png">
                        <h3>Simple</h3>
                        <h5>Server configuration now easy, simple panel, and no pain.</h5>
                    </div>
                    <div class="col-md-4">
                        <img src="/build/image/target.png">
                        <h3>Focus & Secure</h3>
                        <h5>Just focus on your app, its automated panel and secured.</h5>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimoni-page">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <h1>&ldquo;</h1>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="carousel-caption">
                            <h3>Simple to setup, ease of use. Save the day</h3>
                            <h6>Kamarool Karim</h6>
                            <h5>Cool Code</h5>
                        </div>
                    </div>

                    <div class="item">
                        <div class="carousel-caption">
                            <h3>WHoP has been a godsend... fills the gap that existed in previous hosting panel. </h3>
                            <h6>Izzudin Razali</h6>
                            <h5>Infinite Logix</h5>
                        </div>
                    </div>

                    <div class="item">
                        <div class="carousel-caption">
                            <h3>WHoP has saved our company so much time over the last few years by providing us better service with affordable price.</h3>
                            <h6>Ahmad Fikrizaman</h6>
                            <h5>Trip advisor</h5>
                        </div>
                    </div>

                    <div class="item">
                        <div class="carousel-caption">
                            <h3>I never seen such a beautiful and inspiring user experience that make my heart trembling.</h3>
                            <h6>Amir Fazwan</h6>
                            <h5>Terato Tech</h5>
                        </div>
                    </div>
                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="dripicons-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="dripicons-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <section class="feature-page">
            <div class="row">
                <div class="col-md-6">
                    <h3>Simple Installation</h3>
                    <p>
                        Didn't know how to setup your server to be a perfect server for web hosting? Afraid not more, WHoP installation is easier than ever. Run the installation files, and your server will automatically running
                        the best server setting without any configuration.
                    </p>
                    <h3>WHoPlet</h3>
                    <p>
                        WHoPlet is an app point directly to your web application. WHoPlet enable you to setup base application directory including open_basedir. If one of your website is compromise, let it be the only one. With WHoPlet, you can even change your SSL/TLS Certificate on the fly, add new domain and even changing your php version.
                    </p>
                    <h3>File Manager</h3>
                    <p>
                        File manager is one of the essential features in web hosting panel. With WHoP File Manager, you can manage your files via online interface. WHoP File Manager also included text editor with syntax and theme of your selection.
                    </p>
                    <h3>Supervisor &copy;</h3>
                    <p>
                        WHoP have interface to manage supervisor &copy; job. Every client in your server can manage their supervisor &copy; job
                        easily by adding, reloading, and deleting their own job. With the help of the supervisor &copy;, your web application can run queue job in the background without hassle.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <img src="/build/image/screenshot_1.png" class="pull-left">
                </div>
            </div>
        </section>
        <section class="feature-page">
            <div class="row">
                <div class="col-md-6">
                    <img src="/build/image/screenshot_2.png" class="pull-right">
                </div>
                <div class="col-md-6">
                    <h3>Basic Applications</h3>
                    <p>
                        Database Management, Domain &amp; Record Management with DNSSEC, File Management, FTP Management, Email Management and cron is a basic application that should have in any hosting panel. WHoP provided these with simple and clean interface that you can understand easily.
                    </p>
                    <h3>Multi Servers</h3>
                    <p>
                        WHoP allows you to have up to 4 servers running different services. First servers will be running NGiNX &copy; and Apache &copy;. Second server can run database, third can run email service and last one can run dns. With WHoP, you can either have only one, two, three or four servers depending on your infrastructure.
                    </p>
                    <h3>Multi Client</h3>
                    <p>
                        If you are a WHoP admin who owns the server(s), you can add more clients to your server. These clients can have their own WHoPlet, with different php version, Supervisor &copy; settings, ssh, git, composer, shared redis, and even you can set when their account will be terminated according to the client package that you can customize yourself.
                    </p>
                    <h3>Live Log &amp; Statistic</h3>
                    <p>
                        WHoPlet is very special tool crafted for you. It have live access and error log where you can monitor when you are opening your website. WHoPlet also can show you the total access and total error per day for the whole month.
                    </p>
                </div>
            </div>
        </section>
        <section class="screenshot screenshot-sided-page">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h2>The experience you will never forgot.</h2>
                    <h6>We delivered best experience to you, to make your development easier.</h6>
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="/build/image/screenshot_3.png" class="image-left">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="/build/image/screenshot_4.png" class="image-right">
                        </div>
                    </div>
                    <h2>&nbsp;</h2>
                    <h6>&nbsp;.</h6>
                </div>
                <div class="col-lg-6 col-md-6">
                    <h2>&nbsp;</h2>
                    <h6>&nbsp;</h6>
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="/build/image/screenshot_5.png" class="image-left">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="/build/image/screenshot_6.png" class="image-right">
                        </div>
                    </div>
                    <h2>&nbsp;</h2>
                    <h6>&nbsp;.</h6>
                </div>
            </div>
        </section>
    </div>

@stop
